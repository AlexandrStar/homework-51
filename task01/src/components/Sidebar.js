import React from 'react';

const Sidebar = () => {
    return (
        <div className="sidebar">
            <div className="sidenav">
                <a href="#">Home</a>
                <a href="#">Service</a>
                <a href="#">Happy Clients</a>
                <a href="#">Contact Us</a>
            </div>
        </div>

    );
};

export default Sidebar;