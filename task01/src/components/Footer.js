import React from 'react';

const Footer = () => {
    return (
        <footer>
            <div className="container">
                <p>All contents © copyright 2014 Business Theme. All rights reserved Designed by : <a
                    href="#">akhilwebfolio</a></p>
            </div>
        </footer>
    );
};

export default Footer;