import React from 'react';
import Card from "./Card";

const films = [
    {title: 'Крестный отец',
    year: 1972,
    img: 'https://st.kp.yandex.net/images/film_iphone/iphone360_325.jpg',
    link: 'https://www.kinopoisk.ru/film/325/'},
    {title: 'Зеленая миля',
    year: 1999,
    img: 'https://st.kp.yandex.net/images/film_iphone/iphone360_435.jpg',
    link: 'https://www.kinopoisk.ru/film/435/'},
    {title: 'Карты, деньги, два ствола',
    year: 1998,
    img: 'https://st.kp.yandex.net/images/film_iphone/iphone360_522.jpg',
    link: 'https://www.kinopoisk.ru/film/karty-dengi-dva-stvola-1998-522/'}
];

const Contents = () => {
    return (
        <div>
            {films.map((film) => <Card title = {film.title} year = {film.year} img = {film.img} link = {film.link} />
            )}
        </div>
    );
};

export default Contents;