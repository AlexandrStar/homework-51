import React from 'react';

const Header = () => {
    return (
        <div className="top-page">
            <header className="container clearfix">
                <a className="logo-header" href="#">Logo</a>
                <nav className="main-nav">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Service</a></li>
                        <li><a href="#">Happy Clients</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </nav>
            </header>
        </div>
    );
};

export default Header;