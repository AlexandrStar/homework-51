import React from 'react';

const Card = (props) => {
    return (
        <div className='dox'>
            <h3 className='title'>{props.title}</h3>
            <p>{props.year}</p>
            <a className='box-link' href={props.link}><img className='imades' src={props.img} /></a>
        </div>
    );
};

export default Card;